#! /usr/bin/env python3.7
# -*- coding: utf-8
import sqlite3
from datetime import datetime

from PingCheck import PingCheck
from DataBaseSqlite import DataBaseSqlite
# from DataBaseMongoDB import DataBaseMongoDB
from WifiNameFinder import WifiNameFinder
from params import *
# import pymongo

if __name__ == '__main__':

    # With local
    pingcheck = PingCheck(['8.8.8.8'])
    localSqlite3 = DataBaseSqlite(DATABASE_NAME)
    ping_results = pingcheck.run_pings(WifiNameFinder(), localSqlite3)
    pingcheck.commit_results(ping_results, localSqlite3)

    '''
    # Post ping result directly to distant DB
    distantMongo = DataBaseMongoDB(DATABASE_NAME)
    ping_results = pingcheck.run_pings(WifiNameFinder(), distantMongo)
    pingcheck.commit_results(ping_results, distantMongo)
    '''
    

