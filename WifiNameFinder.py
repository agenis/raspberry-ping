#! /usr/bin/env python3.7
# -*- coding: utf-8
import subprocess
import platform


class WifiNameFinder:
    def __init__(self):
        self._wifi_name = "no Wifi or unknown Wifi"
        self.__os_platform = self.__find_os()

    def __find_os(self):
        return platform.system()

    def get_os(self):
        return self.__os_platform

    def find_name(self):
        if self.__os_platform == "Windows":
            call = subprocess.Popen(f"netsh wlan show interfaces", shell=True, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            output, errors = call.communicate()
            wifi_infos = output.decode('Latin-1')
            _wifi_name = wifi_infos.partition("SSID")[2].partition(": ")[2].partition("BSSID")[0].strip()
        elif self.__os_platform == "Linux":
            call = subprocess.Popen(f"iwgetid -r", shell=True, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            output, errors = call.communicate()
            wifi_infos = output.decode('utf-8')
            _wifi_name = wifi_infos.rstrip()
        else:
            return "Unable to get Wifi name"
        if _wifi_name != "":
            return _wifi_name
        else:
            return "no Wifi or unknown Wifi"

