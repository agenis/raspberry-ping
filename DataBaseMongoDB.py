#! /usr/bin/env python3.7
# -*- coding: utf-8

from AbstractDataBase import AbstractDataBase
import pymongo
from params import *
import time


class DataBaseMongoDB(AbstractDataBase):

    def __init__(self, _database_name):
        connect_string = "mongodb://root:root@cluster0-shard-00-00.sdbck.mongodb.net:27017," \
                         "cluster0-shard-00-01.sdbck.mongodb.net:27017,cluster0-shard-00-02.sdbck.mongodb.net:27017/"\
                         + _database_name + "?ssl=true&replicaSet=atlas-xgr1o7-shard-0&authSource=admin&retryWrites" \
                                            "=true&w=majority"
        self._connection = pymongo.MongoClient(connect_string, serverSelectionTimeoutMS=TIMEOUT_DISTANT_DATABASE)
        self._distant_db = self._connection['ping_database']

    def fetch(self):
        _cursor = self._distant_db[TABLE_PINGS_NAME].find()
        result = [doc for doc in _cursor]
        print("found {} rows in database".format(len(result)))
        return result

    def persist(self, _query: str):
        self._distant_db[TABLE_PINGS_NAME].insert_one(_query)
        last = self._distant_db[TABLE_LAST_NAME].find_one()
        self._distant_db[TABLE_LAST_NAME].replace_one(last, {'datetime': time.time()})
        return True

    def build_query(self, ip, result, datetime, wifiname):
        _query = {'id': None, 'ip': ip, 'result': result, 'datetime': datetime, 'wifi_name': wifiname}
        return _query

    def get_last_update(self):
        _last = self._distant_db[TABLE_LAST_NAME].find_one()
        return _last['datetime']


if __name__ == "__main__":
    distantMongoDB = DataBaseMongoDB(DATABASE_NAME_DISTANT)
    query = distantMongoDB.build_query('0.0.0.0', 0, time.time(), 'wtf')
    distantMongoDB.persist(query)
    a = distantMongoDB.fetch()
    print(a)



