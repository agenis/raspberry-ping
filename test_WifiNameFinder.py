#! /usr/bin/env python3.7
# -*- coding: utf-8
import pytest
from WifiNameFinder import WifiNameFinder


def test_find_os():
    wifi_finder = WifiNameFinder()
    # test that os that is returned is eleemnt of "windows" or "linux"
    assert wifi_finder.get_os() in ["Windows", "Linux"]

