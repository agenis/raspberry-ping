#! /usr/bin/env python3.7
# -*- coding: utf-8
import platform

# Parametres pour le script [windows, raspbian]
OS_PLATFORM = platform.system()

SUCCESSFUL_PING_REGEX1 = {'Windows': "re‡us = 1", 'Linux': '1 received'}[OS_PLATFORM]
SUCCESSFUL_PING_REGEX2 = {'Windows': "approximative", 'Linux': ""}[OS_PLATFORM]

PING_PARAMS = {'Windows': "-n 1 -w 1000", 'Linux': "-c 1 -w 2000"}[OS_PLATFORM]

DATABASE_NAME = {'Windows': "ping_database.db", 'Linux': "/home/pi/Documents/raspberry-ping/ping_database.db"}[OS_PLATFORM]
DATABASE_NAME_DISTANT = "ping_database.db"
TABLE_PINGS_NAME = "pings_table"
TABLE_LAST_NAME = "last_updated_table"

TIMEOUT_DISTANT_DATABASE = 5000 # ms
