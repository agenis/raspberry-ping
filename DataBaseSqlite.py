#! /usr/bin/env python3.7
# -*- coding: utf-8

from AbstractDataBase import AbstractDataBase
import sqlite3


class DataBaseSqlite(AbstractDataBase):

    def __init__(self, _database_name):
        self._connection = sqlite3.connect(_database_name)
        self._cursor = self._connection.cursor()
        command1 = """CREATE TABLE IF NOT EXISTS
        pings_table(ping_id INTEGER PRIMARY KEY, ip TEXT, result INTEGER, datetime REAL, wifiname TEXT)"""
        command2 = """CREATE TABLE IF NOT EXISTS
        last_updated_table(last_id INTEGER PRIMARY KEY, datetime REAl)"""
        self._cursor.execute(command1)
        self._cursor.execute(command2)

    def fetch(self):
        nrows_query = self._cursor.execute("SELECT COUNT(*) FROM pings_table")
        print("found {} rows in database".format(nrows_query.fetchone()))
        result = self._cursor.execute("SELECT * FROM pings_table").fetchall()
        return result

    def persist(self, _query: str):
        self._cursor.execute(_query)
        self._connection.commit() 
        return True

    def build_query(self, ip, result, datetime, wifiname):
        query = "INSERT INTO pings_table(ping_id, ip, result, datetime, wifiname) VALUES (null, '{0}', {1}, {2}, '{3}')".format(ip, result, datetime, wifiname)
        return query


