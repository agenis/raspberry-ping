#! /usr/bin/env python3.7
# -*- coding: utf-8

from abc import ABC, abstractmethod


class AbstractDataBase(ABC):

    def __init__(self, _database_name):
        # should instantiate and connect here
        self._database_name = _database_name
        super().__init__()

    @abstractmethod
    def persist(self, _query: str) -> bool:
        pass

    @abstractmethod
    def fetch(self) -> list:
        pass

    @abstractmethod
    def build_query(self):
        pass



