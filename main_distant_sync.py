#! /usr/bin/env python3.7
# -*- coding: utf-8
# import sqlite3
from datetime import datetime

# from PingCheck import PingCheck
from DataBaseSqlite import DataBaseSqlite
from DataBaseMongoDB import DataBaseMongoDB
# from WifiNameFinder import WifiNameFinder
from params import *
import pymongo


if __name__ == '__main__':

    # fetch local DB
    localSqlite3 = DataBaseSqlite(DATABASE_NAME)
    local = localSqlite3.fetch()

    # sync with distant DB or Errorhandling if no internet.
    try:
        distantMongo = DataBaseMongoDB(DATABASE_NAME_DISTANT)
        last_update_time = distantMongo.get_last_update()
        print("Distant database last updated on {}".format(datetime.fromtimestamp(last_update_time)))
        to_be_sync = [elt for elt in local if elt[3] > last_update_time]
        for elt in to_be_sync:
            distantMongo.persist(distantMongo.build_query(elt[1], elt[2], elt[3], elt[4]))
        print("synchronization of {} elements done.".format(len(to_be_sync)))
    except pymongo.errors.ServerSelectionTimeoutError as e:
        print("No internet connection to make the sync. Skipping this sync.")
