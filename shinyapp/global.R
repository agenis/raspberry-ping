library(shiny)
#library(dplyr)
library(plotly)
library(mongolite)

connection_string = "mongodb://reviewer:reviewer@cluster0-shard-00-00.sdbck.mongodb.net:27017,cluster0-shard-00-01.sdbck.mongodb.net:27017,cluster0-shard-00-02.sdbck.mongodb.net:27017/ping_database?ssl=true&replicaSet=atlas-xgr1o7-shard-0&authSource=admin&retryWrites=true&w=majority"

con = mongo(
  collection = "pings_table",
  db = "ping_database",
  url = connection_string,
  verbose = TRUE,
  options = ssl_options()
)

