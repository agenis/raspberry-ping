# raspberry-ping

Project to check and monitor the Wifi box connectivity based on a raspberry 4 and some databases.
It pings a list of IP adresses (here just google) and store logs in a local database. Then, on a regular basis we call a remote database in the cloud to synchronise the local data (if the wifi is on, of course ;-) ).

Branch *develop* is the current uptodate branch. 

Commands on raspberry to start the cron services

`sudo systemctl enable raspberry-ping.service`

`sudo systemctl enable raspberry-ping-sync.service`

`sudo systemctl daemon-reload`

`sudo systemctl start raspberry-ping.service`

`sudo systemctl start raspberry-ping-sync.service`

Do the same for the .timer files. Reboot the rasp and that should begin on start-up.

Architecture:

![Overview of the architecture](architecture.png)