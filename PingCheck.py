#! /usr/bin/env python3.7
# -*- coding: utf-8

import subprocess
from AbstractDataBase import AbstractDataBase
from WifiNameFinder import WifiNameFinder
from params import *
import time


class PingCheck:
    def __init__(self, ip_list=None):
        self.__ip_list = ip_list if ip_list is not None else []
        self.wifi_name = "no Wifi or unknown Wifi"
        self.query_list = None

    def add_ip(self, new_ip):
        self.__ip_list.append(new_ip)

    def get_ip_list(self):
        return self.__ip_list

    def run_pings(self, wifinamefinder: WifiNameFinder, databaseinstance: AbstractDataBase ):
        self.wifi_name = wifinamefinder.find_name()
        query_list = []
        for ip in self.__ip_list:
            call = subprocess.Popen(f"ping {ip} {PING_PARAMS}", shell=True, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            output, errors = call.communicate()
            lines = output.decode('cp1252')
            if SUCCESSFUL_PING_REGEX1 and SUCCESSFUL_PING_REGEX2 in lines:
                query_list.append(databaseinstance.build_query(str(ip), int(1), time.time(), str(self.wifi_name)))
            else:
                query_list.append(databaseinstance.build_query(str(ip), int(0), time.time(), str(self.wifi_name)))
        self.query_list = query_list
        '''
        # log to file
        file = open("logs/log.txt", "a+")
        for _query in query_list:
            file.write(_query + "\n\n")
        '''
        return query_list

    def commit_results(self, query_list, database: AbstractDataBase):
        # commit in database
        for _query in query_list:
            database.persist(_query)




