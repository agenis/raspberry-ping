#! /usr/bin/env python3.7
# -*- coding: utf-8
from PingCheck import *
from DataBaseSqlite import DataBaseSqlite
from params import *
from mock import patch
from WifiNameFinder import WifiNameFinder


def test_addip():
    empty_pingcheck = PingCheck()
    # test that ip_list is empty at first
    assert not empty_pingcheck.get_ip_list()
    # add ip_list and test that size is 2
    empty_pingcheck.add_ip("8.8.8.8")
    empty_pingcheck.add_ip("8.8.4.4")
    assert len(empty_pingcheck.get_ip_list()) == 2
    assert "8.8.4.4" in empty_pingcheck.get_ip_list()


def test_runping():
    localSqlite3 = DataBaseSqlite(DATABASE_NAME)
    pingcheck = PingCheck(['8.8.8.8', '8.8.4.4'])
    ping_results = pingcheck.run_pings(WifiNameFinder(), localSqlite3)
    assert len(ping_results) == 2


@patch.object(WifiNameFinder, "find_name")
def test_commitresults(mock_wnf):
    # mocking wifi name (en fait c'est pas nécessaire de mocker la BDD, car elle est créée localement)
    mock_wnf.return_value = "FLEURDA_mock"
    # unittest
    localSqlite3 = DataBaseSqlite(DATABASE_NAME)
    pingcheck = PingCheck(['8.8.8.8', '8.8.4.4'])
    ping_results = pingcheck.run_pings(WifiNameFinder(), localSqlite3)
    assert len(ping_results) == 2
    initial_db_size = len(localSqlite3.fetch())
    pingcheck.commit_results(ping_results, localSqlite3)
    new_db_size = len(localSqlite3.fetch())
    assert (new_db_size - initial_db_size == 2)



